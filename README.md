For all your building materials, timber & DIY needs.
When you trade with Buildbase, you’ll get everything you need in one place. We take pride in serving the local tradespeople and our communities, working with our customers to get the job done, so you can rely on us to give a brilliant service.

Address: Burrfields Rd, Portsmouth PO3 5NA, United Kingdom

Phone: +44 23 9266 2261

Website: https://www.buildbase.co.uk/storefinder/store/Portsmouth-1161